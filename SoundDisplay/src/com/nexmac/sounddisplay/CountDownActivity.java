package com.nexmac.sounddisplay;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CountDownActivity extends Activity {
	private int countdown = 5;
	private Timer timer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_count_down);

		final TextView textTime = (TextView) findViewById(R.id.text_time);
		textTime.setText("" + countdown);

		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if (countdown == 0) {
					timer.cancel();
					startActivity(true);
				} else {
					runOnUiThread(new Runnable() {
						public void run() {
							countdown -= 1;
							textTime.setText("" + countdown);
						}
					});
				}
			}
		}, 0, 1000);

		Button startButton = (Button) findViewById(R.id.button_start);
		startButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				timer.cancel();
				startActivity(true);
			}
		});

		Button abortButton = (Button) findViewById(R.id.button_abort);
		abortButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(false);
			}
		});
	}

	private void startActivity(boolean goToLastActivity) {
		Intent i = new Intent();
		i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		i.setClass(this, MainActivity.class);
		i.putExtra("goToLastActivity", goToLastActivity);
		startActivity(i);
	}
}
