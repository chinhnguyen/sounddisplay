package com.nexmac.sounddisplay;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class StartupFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater
				.inflate(R.layout.startup_fragment, container);

		Typeface tfArialB = Typeface.createFromAsset(getActivity().getAssets(),
				"arialbd.ttf");

		TextView welcome1 = (TextView) view.findViewById(R.id.welcome_1);
		TextView welcome2 = (TextView) view.findViewById(R.id.welcome_2);
		welcome1.setTypeface(tfArialB);
		welcome2.setTypeface(tfArialB);

		Typeface tfArial = Typeface.createFromAsset(getActivity().getAssets(),
				"arial.ttf");
		TextView welcome3 = (TextView) view.findViewById(R.id.welcome_3);
		TextView welcome4 = (TextView) view.findViewById(R.id.welcome_4);
		TextView welcome5 = (TextView) view.findViewById(R.id.welcome_5);
		TextView welcome6 = (TextView) view.findViewById(R.id.welcome_6);
		TextView welcome7 = (TextView) view.findViewById(R.id.welcome_7);
		welcome3.setTypeface(tfArial);
		welcome4.setTypeface(tfArial);
		welcome5.setTypeface(tfArial);
		welcome6.setTypeface(tfArial);
		welcome7.setTypeface(tfArial);

		final GestureDetector detector = new GestureDetector(getActivity(),
				new GestureDetector.SimpleOnGestureListener() {
					@Override
					public boolean onDoubleTapEvent(MotionEvent e) {
						view.setVisibility(View.GONE);
						return false;
					}
				});
		view.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return detector.onTouchEvent(event);
			}
		});

		return view;
	}

}
