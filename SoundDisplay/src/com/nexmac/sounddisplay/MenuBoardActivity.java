package com.nexmac.sounddisplay;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewFlipper;

import com.nexmac.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MenuBoardActivity extends Activity {
	private SharedPreferences preferences;

	private ViewFlipper imageSlideShow;
	private VideoView videoView;
	private WebView webView;
	private TextView adText;

	private boolean activated;
	private String passText;

	private boolean shouldSlideshowContinue;
	// Handler for running slide show
	private Handler slideshowHandler = new Handler();

	// Runnable for running slide show
	private Runnable slideshowRunnable = new Runnable() {

		@Override
		public void run() {
			if (!shouldSlideshowContinue)
				return;
			// Show next image
			imageSlideShow.showNext();

			ImageView imageView = (ImageView) imageSlideShow.getCurrentView();
			File f = (File) imageView.getTag();
			if (f == null)
				return;

			// Load images using Image Loader
			ImageLoader.getInstance().displayImage(
					"file://" + f.toURI().getPath(), imageView);
			if (imageSlideShow.getChildCount() > 1)
				// Raise next event
				slideshowHandler.postDelayed(slideshowRunnable, 5000);
		}
	};

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// http://stackoverflow.com/questions/2039555/how-to-get-an-android-wakelock-to-work
		// Keep the screen always ON
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LOW_PROFILE);

		setContentView(R.layout.activity_menu_board);

		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);

		activated = preferences.getBoolean("menuboard-activated", false);

		adText = (TextView) findViewById(R.id.text_ad);
		if (activated)
			adText.setVisibility(View.GONE);

		// Playing slideshow or video
		boolean slideshowMode = getIntent().getBooleanExtra("slideshowMode",
				false);
		if (slideshowMode) {
			initSlideshow(getIntent().getStringExtra("photoPath"));
		} else {
			boolean videoMode = getIntent().getBooleanExtra("videoMode", false);
			if (videoMode) {
				initVideo(getIntent().getStringExtra("videoPath"));
			} else {
				initWeb(getIntent().getStringExtra("url"));
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		preferences.edit().putInt("lastScreen", 4).commit();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
			acceptTypedNumber(keyCode - KeyEvent.KEYCODE_0);
		} else if (keyCode >= KeyEvent.KEYCODE_NUMPAD_0
				&& keyCode <= KeyEvent.KEYCODE_NUMPAD_9) {
			acceptTypedNumber(keyCode - KeyEvent.KEYCODE_NUMPAD_0);
		}
		return super.onKeyUp(keyCode, event);
	}

	private void acceptTypedNumber(int number) {
		if (!activated) {
			passText = passText + number;
			if (passText.length() > 6) {
				passText = passText.substring(passText.length() - 3);
			}
			if (passText.equals("122710")) {
				activated = true;
				preferences.edit().putBoolean("menuboard-activated", true)
						.commit();
				adText.setVisibility(View.GONE);
			}
		}
	}

	@SuppressLint("DefaultLocale")
	private void initSlideshow(String path) {
		if (TextUtils.isEmpty(path))
			return;
		// Init slide shod
		imageSlideShow = (ViewFlipper) findViewById(R.id.view_slide_show);
		imageSlideShow.setVisibility(View.VISIBLE);
		// Get images folder
		File file = new File(path);
		if (!file.exists())
			return;

		File[] files;
		if (file.isDirectory()) {
			files = file.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					String lowercase = name.toLowerCase();
					return lowercase.endsWith(".png")
							|| lowercase.endsWith(".jpg")
							|| lowercase.endsWith(".gif");
				}
			});
		} else {
			String lowercase = file.getName().toLowerCase();
			if (!(lowercase.endsWith(".png") || lowercase.endsWith(".jpg") || lowercase
					.endsWith(".gif"))) {
				return;
			}
			files = new File[] { file };
		}

		if (files == null || files.length == 0)
			return;
		addFlipperImages(imageSlideShow, files);

		imageSlideShow.setInAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_in));
		imageSlideShow.setOutAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_out));

		// Begin the timing
		shouldSlideshowContinue = true;
		slideshowHandler.postDelayed(slideshowRunnable, 500);
	}

	private int videoIndex = 0;

	@SuppressLint("DefaultLocale")
	private void initVideo(final String path) {
		if (TextUtils.isEmpty(path))
			return;
		videoView = (VideoView) findViewById(R.id.view_video);
		videoView.setVisibility(View.VISIBLE);

		// Get images folder
		File file = new File(path);
		if (!file.exists())
			return;
		File[] videofiles;
		if (file.isDirectory()) {
			videofiles = file.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					String lowercase = name.toLowerCase();
					return lowercase.endsWith(".mp4")
							|| lowercase.endsWith(".3gp")
							|| lowercase.endsWith(".ogg")
							|| lowercase.endsWith(".mpg");
				}
			});
		} else {
			String lowercase = file.getName().toLowerCase();
			if (!(lowercase.endsWith(".mp4") || lowercase.endsWith(".3gp") || lowercase
					.endsWith(".ogg") || lowercase.endsWith(".mpg"))) {
				return;
			}
			videofiles = new File[] { file };
		}

		if (videofiles == null || videofiles.length == 0)
			return;

		try {
			// Sort by name
			Arrays.sort(videofiles);

			final File[] files = videofiles;
			// Register on completion listener to play next video
			videoView.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					videoIndex++;
					if (videoIndex == files.length)
						videoIndex = 0;
					videoView.setVideoPath(files[videoIndex].toString());
					videoView.start();
				}
			});
			// Play the first video
			videoView.setVideoPath(files[videoIndex].toString());
			videoView.start();
		} catch (Exception e) {
			Log.e("MenuBoard", "Error occurred during playing videos", e);
		}
	}

	private void fallbackToSlideshow(String message) {
		initSlideshow(getIntent().getStringExtra("photoPath"));
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWeb(final String url) {
		if (!Utils.isConnected(this)) {
			fallbackToSlideshow("Network is not available, fallback to slideshow!");
			return;
		}
		if (TextUtils.isEmpty(url)) {
			fallbackToSlideshow("URL was not given, fallback to slideshow!");
			return;
		}
		webView = (WebView) findViewById(R.id.view_web);
		webView.setVisibility(View.VISIBLE);
		webView.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					onKeyUp(keyCode, event);
				}
				return false;
			}
		});

		// http://stackoverflow.com/questions/4066438/android-webview-how-to-handle-redirects-in-app-instead-of-opening-a-browser
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				webView.setVisibility(View.GONE);
				fallbackToSlideshow("Could not load URL " + failingUrl
						+ ", fallback to slideshow!");
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.clearCache(true);
			}
		});

		// http://stackoverflow.com/questions/3916330/android-webview-webpage-should-fit-the-device-screen
		webView.setInitialScale(1);
		webView.setPadding(0, 0, 0, 0);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webView.setScrollbarFadingEnabled(false);
		// http://stackoverflow.com/questions/7537701/prevent-android-webview-caching-data
		Map<String, String> noCacheHeaders = new HashMap<String, String>(2);
		noCacheHeaders.put("Pragma", "no-cache");
		noCacheHeaders.put("Cache-Control", "no-cache");
		webView.loadUrl(url, noCacheHeaders);
	}

	@SuppressLint("DefaultLocale")
	private void addFlipperImages(final ViewFlipper flipper, File[] files) {
		// ImageLoader.getInstance().clearMemoryCache();
		// Make sure the folder exists
		if (files.length == 0)
			return;
		// Get count of files
		int imageCount = files.length;
		// Prepare the params for adding new image view
		ViewFlipper.LayoutParams params = new ViewFlipper.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		// Add all images using Image loader
		for (int i = 0; i < imageCount; i++) {
			File f = files[i];
			// Accept images file only
			if (f.isDirectory()
					|| !(f.getName().toLowerCase().endsWith(".jpg")
							|| f.getName().toLowerCase().endsWith(".png") || f
							.getName().toLowerCase().endsWith(".gif")))
				continue;

			// New image view
			final ImageView imageView = new ImageView(this);
			// For UIL co calculate the size
			imageView.setMaxWidth(metrics.widthPixels);
			imageView.setMaxHeight(metrics.heightPixels);
			// Should stretch to fit screen
			imageView.setScaleType(ScaleType.FIT_XY);
			// Add it to flipper
			imageView.setLayoutParams(params);
			imageView.setTag(f);
			// Add to flipper only when it is success
			flipper.addView(imageView);
		}
	}

}
