package com.nexmac.sounddisplay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

	// private static final String TAG = "BootReceiver";

	@Override
	public void onReceive(final Context context, Intent intent) {
		// Log.d(TAG, "Starting MainActivity");
		Intent i = new Intent();
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addFlags(Intent.FLAG_FROM_BACKGROUND);
		i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		i.setClass(context, CountDownActivity.class);
		context.startActivity(i);
		// Log.d(TAG, "Done starting MainActivity");
	}
}
