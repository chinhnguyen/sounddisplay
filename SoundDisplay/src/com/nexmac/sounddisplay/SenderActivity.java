package com.nexmac.sounddisplay;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import zxing.library.DecodeCallback;
import zxing.library.ZXingFragment;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.zxing.Result;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.nexmac.Utils;
import com.nexmac.sounddisplay.OrderDetailFragment.OrderListener;
import com.nexmac.sounddisplay.models.Order;

public class SenderActivity extends SoundActivity {
	// List of number on top panel
	private ArrayList<Order> orders;

	// Textbox to hold the value of current number.
	private TextView numberText;

	// The scan button
	private ToggleButton scanButton;

	// The scanner
	private ZXingFragment scanner;

	// Top panel list view
	private HorizontalListView orderList;
	private OrderListAdapter orderListAdapter;

	// List of text views
	private TextView[] bottomTextViews = new TextView[6];
	// List of received and processed number
	private ArrayList<Order> secondOrders = new ArrayList<Order>();

	/**
	 * Handle click on control pad button.
	 */
	private OnClickListener mControlPadButtonOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String t = numberText.getText().toString();
			t = t + v.getTag();
			if (t.length() > 3) {
				t = t.substring(t.length() - 3);
			}
			numberText.setText(t);
		}
	};

	private void updateDetailPanel(final Order order, boolean showPrompt) {
		// Make sure only processing the true order
		if (order == null)
			return;
		// Get the fragment inside place holder
		Fragment f = getSupportFragmentManager().findFragmentById(R.id.scanner);

		OrderDetailFragment orderDetail;
		if (f == null || f.getClass() != OrderDetailFragment.class) {
			// Not yet an Order Detail fragment
			// Then, create a new one
			orderDetail = new OrderDetailFragment();
			orderDetail.order = order;

			// Touch to send to server
			orderDetail.orderListener = new OrderListener() {
				@Override
				public void processOrder(Order order) {
					// Remove from top panel
					orders.remove(order);
					orderListAdapter.notifyDataSetChanged();
					// Send to server
					sendToServer(order.order);
					// Play the number
					playNumber(order.order);
					// Move to bottomr
					addOrderTo2ndList(order);
				}
			};

			// Replace content
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.replace(R.id.scanner, orderDetail);
			transaction.commit();
		} else {
			// Already an order detail fragment
			orderDetail = (OrderDetailFragment) f;
			// Just update content
			orderDetail.updateOrder(order);
		}
		// Show/hide the promt text
		if (orderDetail.promptText != null)
			orderDetail.promptText.setVisibility(showPrompt ? View.VISIBLE
					: View.GONE);
		// Move to scan-off state
		scanButton.setChecked(false);
	}

	/**
	 * Add an order to bottom list for 2nd try.
	 * 
	 * @param order
	 */
	private void addOrderTo2ndList(Order order) {
		if (secondOrders.contains(order))
			return;
		// Remove the last item if it is more than 6 elements
		if (secondOrders.size() == 6)
			secondOrders.remove(secondOrders.size() - 1);
		// Add to list of displaying number
		secondOrders.add(0, order);
		// Update display list
		for (int i = 0; i < 6; i++) {
			if (i < secondOrders.size()) {
				bottomTextViews[i].setText(String.format("%d",
						secondOrders.get(i).order));
				bottomTextViews[i].setTag(secondOrders.get(i));
			} else {
				bottomTextViews[i].setText("");
				bottomTextViews[i].setTag(null);
			}
		}
	}

	/**
	 * Handle click on button of top panel.
	 */
	private OnItemClickListener mTopPanelItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			final Order order = orders.get(position);

			Order currentOrder = null;
			// Get the fragment inside place holder
			Fragment f = getSupportFragmentManager().findFragmentById(
					R.id.scanner);
			if (f != null && f.getClass() == OrderDetailFragment.class) {
				currentOrder = ((OrderDetailFragment) f).order;
			}
			updateDetailPanel(order, false);
			if (currentOrder == order) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(
						SenderActivity.this);
				dialog.setMessage(R.string.confirm_remove);
				dialog.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Remove from top panel
								orders.remove(order);
								orderListAdapter.notifyDataSetChanged();

							}
						});
				dialog.setNegativeButton(R.string.no, null);
				dialog.show();
			}
		}
	};

	/**
	 * Handle CLR click.
	 */
	private OnClickListener mClearClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!TextUtils.isEmpty(numberText.getText())) {
				numberText.setText("");
				return;
			}

			DialogInterface.OnClickListener yesOnClick = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Clear top list
					orders.clear();
					orderListAdapter.notifyDataSetChanged();

					// Clear bottom list
					secondOrders.clear();
					for (int i = 0; i < 6; i++) {
						bottomTextViews[i].setText("");
						bottomTextViews[i].setTag(null);
					}

					// Clear content
					Fragment f = getSupportFragmentManager().findFragmentById(
							R.id.scanner);
					if (f != null) {
						FragmentTransaction transaction = getSupportFragmentManager()
								.beginTransaction();
						transaction.remove(f);
						transaction.commit();
					}
				}
			};

			AlertDialog.Builder dialog = new AlertDialog.Builder(
					SenderActivity.this);
			dialog.setMessage(
					"Do you really want to clear all the items from both lists?")
					.setNegativeButton("No", null)
					.setPositiveButton("Yes", yesOnClick);
			dialog.show();
		}
	};

	/**
	 * Handle OK click.
	 */
	private OnClickListener mOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String strNumber = numberText.getText().toString().trim();
			if (strNumber.length() == 0)
				return;
			try {
				int number = Integer.parseInt(strNumber);
				// Play sound
				playNumber(number);
				// Send to server
				sendToServer(number);
			} catch (NumberFormatException nfe) {
			}
			// Clear
			numberText.setText("");
		}
	};

	/**
	 * Enable scanning view
	 */
	private OnCheckedChangeListener mScanCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				enableScan();
			} else {
				disableScan();
			}
		}

	};

	/**
	 * Listen to decoding result.
	 */
	private DecodeCallback scannedListener = new DecodeCallback() {

		@Override
		public void handleBarcode(Result result, Bitmap arg1, float arg2) {
			String r = result.getText();
			if (TextUtils.isEmpty(r))
				return;
			r = r.trim();
			if (TextUtils.isEmpty(r))
				return;
			if (r.length() > 3)
				r = r.substring(r.length() - 3); // Get last 3 characters
			try {
				int number = Integer.parseInt(r);
				// Play sound
				playNumber(number);
				// Send to server
				sendToServer(number);
			} catch (NumberFormatException nfe) {
			}
		}
	};

	/*
	 * Listen to number over the network
	 */
	private OrderDatagramListener orderDatagramListener;

	private SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sender);

		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);

		numberText = (TextView) findViewById(R.id.text_number);
		numberText.setText("");

		// Init blank list
		orders = new ArrayList<Order>();
		// Create the adapter
		orderListAdapter = new OrderListAdapter(this, orders);
		// Assign to list
		orderList = (HorizontalListView) findViewById(R.id.panel_top);
		orderList.setAdapter(orderListAdapter);
		orderList.setOnItemClickListener(mTopPanelItemClickListener);

		for (int i = 0; i < 10; i++) {
			Integer id = getResources().getIdentifier("button_" + i, "id",
					getBaseContext().getPackageName());
			Button b = (Button) findViewById(id);
			b.setTag(i);
			b.setOnClickListener(mControlPadButtonOnClickListener);
		}

		Button clearButton = (Button) findViewById(R.id.button_clr);
		clearButton.setOnClickListener(mClearClickListener);

		Button okButton = (Button) findViewById(R.id.button_ok);
		okButton.setOnClickListener(mOkClickListener);

		scanButton = (ToggleButton) findViewById(R.id.button_scan);
		scanButton.setOnCheckedChangeListener(mScanCheckedChangeListener);

		// Init the list of text views
		for (int i = 1; i <= 6; i++) {
			Integer id = getResources().getIdentifier("text_" + i, "id",
					getBaseContext().getPackageName());
			TextView v = (TextView) findViewById(id);
			v.setText("");
			bottomTextViews[i - 1] = v;
			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Order order = (Order) v.getTag();
					if (order == null)
						return;
					updateDetailPanel(order, true);
					// Play
					playNumber(order.order);
					// Send to server
					sendToServer(order.order);
				}
			});
		}

		// Get the saved value
		String json = preferences.getString("orders", "");
		if (json.length() > 0) {
			Order[] ods = Order.arrayFromJson(json);
			if (ods != null)
				for (int i = 0; i < ods.length; i++) {
					orders.add(ods[i]);
				}
		}
		preferences.edit().remove("orders");

		// Begin order listener thread
		orderListener.start();

//		// FAKE Data
//		for (int i = 0; i < 20; i++) {
//			Order order = new Order();
//			order.order = 887 + i;
//			OrderItem[] items = new OrderItem[] {};
//			for (int j = 0; j < 3; j++) {
//				OrderItem oi = new OrderItem();
//				oi.index = j;
//				oi.name = "Test " + j;
//				oi.quantity = 20 + j;
//			}
//			order.description = items;
//			order.note = "NOTE TEST";
//			orderHandler.post(new OrderProcessor(order));
//		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (orderDatagramListener == null) {
			// Run number datagram listener's thread
			orderDatagramListener = new OrderDatagramListener();
			orderDatagramListener.start();
		}
		preferences.edit().putInt("lastScreen", 2).commit();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (orderDatagramListener != null) {
			orderDatagramListener.stopServer();
			orderDatagramListener = null;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (orderDatagramListener != null) {
			orderDatagramListener.stopServer();
			orderDatagramListener = null;
		}

		preferences
				.edit()
				.putString("orders",
						Order.toJson(orders.toArray(new Order[] {}))).commit();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putParcelableArray("orders",
				orders.toArray(new Order[] {}));
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		Parcelable[] savedOrders = savedInstanceState
				.getParcelableArray("orders");
		for (int i = 0; i < savedOrders.length; i++) {
			orders.add((Order) savedOrders[i]);
		}
	}

	@Override
	protected void onFinishPlayingSound(Integer number) {
		if (scanButton.isChecked() && scanner != null)
			// Restart the scanner
			scanner.restartScanningIn(200);
	}

	/**
	 * Disable scanning.
	 */
	private void disableScan() {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.remove(scanner);
		transaction.commit();

		scanner = null;
	}

	/**
	 * Enable scanning views
	 */
	private void enableScan() {
		scanner = new ZXingFragment();
		scanner.setDecodeCallback(scannedListener);
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.scanner, scanner);
		transaction.commit();
	}

	private class ViewHolder {
		TextView textView;
	}

	private class OrderListAdapter extends ArrayAdapter<Order> {
		public OrderListAdapter(Context context, List<Order> orders) {
			super(context, 0, orders);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (view == null) {
				LayoutInflater mInflater = LayoutInflater.from(getContext());
				view = mInflater.inflate(R.layout.listitem_number, parent,
						false);
				holder = new ViewHolder();
				holder.textView = (TextView) view.findViewById(R.id.textView);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			Order order = getItem(position);
			holder.textView.setText(order.order.toString());

			return view;
		}
	}

	// Handling the received (not yet processed) numbered
	private ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(
			1000);

	/**
	 * Display status of sender's server socket.
	 * 
	 * @param message
	 */
	private void setStatus(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(SenderActivity.this, message, Toast.LENGTH_LONG)
						.show();
			}
		});
	}

	/**
	 * For handling order received from hosts.
	 */
	private Handler orderHandler = new Handler();

	/**
	 * Order processor: Add to list
	 */
	private class OrderProcessor implements Runnable {
		Order order;

		public OrderProcessor(Order order) {
			this.order = order;
		}

		@Override
		public void run() {
			synchronized (orders) {
				int i = 0;
				for (; i < orders.size(); i++) {
					Order o = orders.get(i);
					if (o.order == order.order)
						break;
				}
				if (i < orders.size())
					orders.set(i, order);
				else
					orders.add(0, order);
			}

			// Reload view.
			orderListAdapter.notifyDataSetChanged();

			// Update the current displaying if the received number is the same
			Fragment f = getSupportFragmentManager().findFragmentById(
					R.id.scanner);
			if (f != null && f.getClass() == OrderDetailFragment.class) {
				OrderDetailFragment orderDetail = (OrderDetailFragment) f;
				if (orderDetail.order.order == order.order) {
					orderDetail.updateOrder(order);
				}
			}
		}
	};

	/**
	 * Listen for new order and do any processing possible
	 */
	private Thread orderListener = new Thread() {

		@Override
		public void run() {
			while (true) {
				try {
					// Take the last order out of queue if there is already
					// order in here
					// OR wait for the new number to come
					final String orderJson = queue.take();
					// Get the object
					Order order = Order.fromJson(orderJson);
					synchronized (this) {
						// Inform order handler about a new order
						orderHandler.post(new OrderProcessor(order));
					}
				} catch (InterruptedException e) {
					// Should never goes here
				}
			}
		}
	};

	/**
	 * Listen to number from the network.
	 */
	private class OrderDatagramListener extends Thread {
		DatagramSocket serverSocket;

		public void stopServer() {
			if (serverSocket != null)
				serverSocket.close();
		}

		@Override
		public void run() {

			try {
				serverSocket = new DatagramSocket(SENDER_PORT);
			} catch (SocketException e) {
				// Log
				Log.e("Sender", e.toString());
				// Further UI exception handling
				// Update server info text
				setStatus(String.format("Could not start server on %s:%d",
						Utils.getIPAddress(true), SENDER_PORT));
				return;
			}

			setStatus(String.format("Listening on %s:%d",
					Utils.getIPAddress(true), SENDER_PORT));

			// Int number buffer
			byte[] buf = new byte[1024];
			// Datagram packet to get an int
			DatagramPacket pack = new DatagramPacket(buf, buf.length);
			while (!serverSocket.isClosed()) {
				try {
					// Wait for package
					serverSocket.receive(pack);
					// Do nothing the socket is closed
					if (serverSocket.isClosed())
						break;
					String orderJson = new String(buf, "UTF-8");
					try {
						// Put the number of queue for processing
						// OR wait for the queue has blank space
						queue.put(orderJson);
						// Update text (only for DEBUG)
						// updateQueueNumbersInfo();
					} catch (InterruptedException e) {
					}
				} catch (IOException e) {
					// Log
					e.printStackTrace();
					// Further UI exception handling
					handleException(e);
				}
			}
		}
	}
}
