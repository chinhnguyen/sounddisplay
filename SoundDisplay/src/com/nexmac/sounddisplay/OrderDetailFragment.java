package com.nexmac.sounddisplay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nexmac.sounddisplay.models.Order;
import com.nexmac.sounddisplay.models.OrderItem;

public class OrderDetailFragment extends Fragment {
	public Order order;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_order_detail, container,
				false);
	}

	TextView number;
	TextView note;
	TextView description;
	
	public TextView promptText;

	public interface OrderListener {
		void processOrder(Order order);
	}

	public OrderListener orderListener;

	@Override
	public void onResume() {
		super.onResume();

		if (order == null)
			return;

		if (orderListener != null) {
			ScrollView sv = (ScrollView) getView().findViewById(
					R.id.view_detail);
			sv.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_UP) {
						if (orderListener != null)
							orderListener.processOrder(order);
					}
					return false;
				}
			});
		}

		promptText = (TextView)getView().findViewById(R.id.text_prompt);
		number = (TextView) getView().findViewById(R.id.text_number);
		note = (TextView) getView().findViewById(R.id.text_note);
		description = (TextView) getView().findViewById(R.id.text_description);

		updateOrder(this.order);
	}

	public void updateOrder(Order order) {
		this.order = order;
		number.setText("ORDER # " + order.order.toString());
		note.setText(order.note);
		StringBuilder str = new StringBuilder();
		for (OrderItem i : order.description) {
			str.append(i.quantity);
			if (i.name != null)
				str.append(" ").append(i.name);
			str.append("\n");
		}
		description.setLines(order.description.length);
		description.setText(str);
	}
}
