package com.nexmac.sounddisplay;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;

public class SimpleSenderActivity extends SoundActivity {
	private SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_sender);

		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);

		int padding = (int) getResources().getDimension(
				R.dimen.simple_sender_button_padding);
		int textsize = (int) getResources().getDimension(
				R.dimen.simple_sender_button_text_size);

		View.OnClickListener clickListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int value = (Integer) v.getTag();
				// Play sound
				playNumber(value);
				// Send to server
				sendToServer(value);
			}
		};

		TableLayout table = (TableLayout) findViewById(R.id.table_control_pad);
		// Clear design views
		table.removeAllViews();

		for (int i = 0; i < 10; i++) {
			LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
			lp.setMargins(0, padding, 0, padding);
			TableRow tr = new TableRow(this);
			table.addView(tr, lp);

			for (int j = 0; j < 10; j++) {
				TableRow.LayoutParams rlp = new TableRow.LayoutParams(0,
						TableRow.LayoutParams.MATCH_PARENT, 1);
				rlp.setMargins(padding, 0, padding, 0);
				Button b = new Button(this);
				int value = i * 10 + j + 1;
				b.setText(String.format("%d", value));
				b.setTag(value);
				b.setTextSize(textsize);
				tr.addView(b, rlp);
				b.setOnClickListener(clickListener);
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		preferences.edit().putInt("lastScreen", 1).commit();
	}
}
