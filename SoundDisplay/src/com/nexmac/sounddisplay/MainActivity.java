package com.nexmac.sounddisplay;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

import com.lamerman.FileDialog;
import com.lamerman.SelectionMode;

public class MainActivity extends Activity {

	/**
	 * Open sender screen.
	 */
	private OnClickListener senderOnClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (senderMode.isChecked())
				startAdvanceSender();
			else
				startSimpleSender();
		}
	};

	/**
	 * Quit application.
	 */
	private OnClickListener exitOnClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	};

	/**
	 * Listen to auto reboot option on/off
	 */
	private OnCheckedChangeListener autoRebootModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			SoundDisplayApp app = (SoundDisplayApp) MainActivity.this
					.getApplication();
			if (isChecked)
				app.registerAutoReboot();
			else
				app.unregisterAutoReboot();
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	/**
	 * Reboot device
	 */
	private OnClickListener rebootOnClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			try {
				// Runtime.getRuntime().exec(
				// new String[] { "su", "-c",
				// "busybox killall system_server" });
				Runtime.getRuntime()
						.exec(new String[] { "su", "-c", "reboot" });
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * Ask user to confirm before changing a path.
	 */
	private void confirmChangingPath(final int id) {
		// Build confirm dialog for path selection
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		String path = "";
		if (id == 0)
			path = filePath;
		else if (id == 1)
			path = photoPath;
		else if (id == 2)
			path = videoPath;
		else if (id == 10)
			path = menuPhotoPath;
		else if (id == 11)
			path = menuVideoPath;
		String message = getString(R.string.change_path_message, path);
		builder.setMessage(message).setTitle(R.string.change_path_title);
		builder.setPositiveButton(R.string.yes,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startFileDialogActivity(id);
					}
				});
		builder.setNegativeButton(R.string.no, null);
		AlertDialog dialog = builder.create();
		// Confirm changing before changing
		dialog.show();
	}

	/**
	 * Listen to media play mode change
	 */
	private OnCheckedChangeListener normalModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnCheckedChangeListener prefixModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				final EditText prefixInput = new EditText(MainActivity.this);
				prefixInput.setText(prefix);
				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Update prefix")
						.setMessage("Please enter the prefix text.")
						.setView(prefixInput)
						.setPositiveButton("Save",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										prefix = prefixInput.getText()
												.toString();
										preferences.edit()
												.putString("prefix", prefix)
												.commit();
									}
								})
						.setNegativeButton("Close",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// Do nothing.
									}
								}).show();
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnCheckedChangeListener suffixModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				final EditText suffixInput = new EditText(MainActivity.this);
				suffixInput.setText(suffix);
				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Update suffix")
						.setMessage("Please enter the suffix text.")
						.setView(suffixInput)
						.setPositiveButton("Save",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										suffix = suffixInput.getText()
												.toString();
										preferences.edit()
												.putString("suffix", suffix)
												.commit();
									}
								})
						.setNegativeButton("Close",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// Do nothing.
									}
								}).show();
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	/**
	 * Listen to media play mode change
	 */
	private OnCheckedChangeListener mediaPlayModeChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				// mediaPlayMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size_small));
				if (TextUtils.isEmpty(filePath)) {
					// Start selection dialog right away if there is no path yet
					startFileDialogActivity(0);
				} else {
					confirmChangingPath(0);
				}
			} else {
				// mediaPlayMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size));
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	/**
	 * Listen to media play mode change
	 */
	private OnCheckedChangeListener slideshowModeChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				videoMode.setChecked(false);
				urlMode.setChecked(false);
				// slideshowMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size_small));
				if (TextUtils.isEmpty(photoPath)) {
					// Start selection dialog right away if there is no path yet
					startFileDialogActivity(1);
				} else {
					confirmChangingPath(1);
				}
			} else {
				// slideshowMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size));
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	/**
	 * Listen to media play mode change
	 */
	private OnCheckedChangeListener videoModeChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				slideshowMode.setChecked(false);
				urlMode.setChecked(false);
				// videoMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size_small));
				if (TextUtils.isEmpty(videoPath)) {
					// Start selection dialog right away if there is no path yet
					startFileDialogActivity(2);
				} else {
					confirmChangingPath(2);
				}
			} else {
				// videoMode.setTextSize(getResources().getDimension(
				// R.dimen.main_option_button_text_size));
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnCheckedChangeListener urlModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				slideshowMode.setChecked(false);
				videoMode.setChecked(false);
				final EditText urlInput = new EditText(MainActivity.this);
				if (urlPath == null || urlPath.length() == 0)
					urlInput.append("http://");
				else
					urlInput.setText(urlPath);

				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Update output image url")
						.setMessage("Please enter the image url.")
						.setView(urlInput)
						.setPositiveButton("Save",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										urlPath = urlInput.getText().toString()
												.trim();
										if (!urlPath.startsWith("http://")) {
											urlPath = "http://" + urlPath;
										}
										preferences.edit()
												.putString("urlPath", urlPath)
												.commit();
									}
								})
						.setNegativeButton("Close",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// Do nothing.
									}
								}).show();
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	/**
	 * Listen to media play mode change
	 */
	private OnCheckedChangeListener menuSlideshowModeChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				menuVideoMode.setChecked(false);
				menuUrlMode.setChecked(false);
				if (TextUtils.isEmpty(menuPhotoPath)) {
					// Start selection dialog right away if there is no path yet
					startFileDialogActivity(10);
				} else {
					confirmChangingPath(10);
				}
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnCheckedChangeListener menuVideoModeChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				menuSlideshowMode.setChecked(false);
				menuUrlMode.setChecked(false);
				if (TextUtils.isEmpty(videoPath)) {
					// Start selection dialog right away if there is no path yet
					startFileDialogActivity(11);
				} else {
					confirmChangingPath(11);
				}
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnCheckedChangeListener menuUrlModeChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				menuSlideshowMode.setChecked(false);
				menuVideoMode.setChecked(false);
				final EditText urlInput = new EditText(MainActivity.this);
				if (menuUrlPath == null || menuUrlPath.length() == 0)
					urlInput.append("http://");
				else
					urlInput.setText(menuUrlPath);

				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Update menu board image url")
						.setMessage("Please enter the image url.")
						.setView(urlInput)
						.setPositiveButton("Save",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										menuUrlPath = urlInput.getText()
												.toString().trim();
										if (!menuUrlPath.startsWith("http://")) {
											menuUrlPath = "http://"
													+ menuUrlPath;
										}
										preferences
												.edit()
												.putString("menuUrlPath",
														menuUrlPath).commit();
									}
								})
						.setNegativeButton("Close",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// Do nothing.
									}
								}).show();
			}
			preferences.edit()
					.putBoolean((String) buttonView.getTag(), isChecked)
					.commit();
		}
	};

	private OnSeekBarChangeListener volumeChangeListener = new OnSeekBarChangeListener() {

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			preferences.edit().putInt((String) seekBar.getTag(), progress)
					.commit();
		}
	};

	/**
	 * Open folder selection dialog.
	 */
	private void startFileDialogActivity(int id) {
		Intent intent = new Intent(MainActivity.this, FileDialog.class);
		intent.putExtra(FileDialog.START_PATH, Environment
				.getExternalStorageDirectory().getPath());
		// can user select directories or not
		intent.putExtra(FileDialog.CAN_SELECT_DIR, true);
		intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_OPEN);
		startActivityForResult(intent, id);
	}

	private ToggleButton senderMode;
	private ToggleButton mediaPlayMode;
	private ToggleButton slideshowMode;
	private ToggleButton videoMode;
	private ToggleButton urlMode;
	private ToggleButton prefixMode;
	private ToggleButton suffixMode;
	private ToggleButton debugMode;

	private ToggleButton menuSlideshowMode;
	private ToggleButton menuVideoMode;
	private ToggleButton menuUrlMode;

	private SeekBar backgroundVolume;

	private String prefix;
	private String suffix;

	// Media file path
	private String filePath;
	private String photoPath;
	private String videoPath;
	private String urlPath;

	private String menuPhotoPath;
	private String menuVideoPath;
	private String menuUrlPath;

	private SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// try {
		// // int s =
		// // android.provider.Settings.System.getInt(getContentResolver(),
		// // "audio.routing");
		// int s1 = android.provider.Settings.Secure.getInt(
		// getContentResolver(), "audio.routing");
		// } catch (SettingNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);

		if (preferences.getBoolean("FIRST_RUN", true)) {
			preferences.edit().putBoolean("FIRST_RUN", false).commit();
		} else {
			FragmentManager fm = getFragmentManager();
			Fragment f = fm.findFragmentById(R.id.startup_panel);
			fm.beginTransaction().hide(f).commit();
		}

		senderMode = (ToggleButton) findViewById(R.id.toggle_sender_mode);
		senderMode.setOnCheckedChangeListener(normalModeChangeListener);
		senderMode.setTag("senderMode");
		senderMode.setChecked(preferences.getBoolean("senderMode", false));

		mediaPlayMode = (ToggleButton) findViewById(R.id.button_media_folder);
		mediaPlayMode.setTag("mediaPlayMode");
		mediaPlayMode
				.setChecked(preferences.getBoolean("mediaPlayMode", false));
		filePath = preferences.getString("filePath", "");
		boolean isChecked = preferences.getBoolean("mediaPlayMode", false);
		if (isChecked)
			mediaPlayMode.setTextOn(getString(R.string.media_folder) + "\n"
					+ filePath);
		mediaPlayMode.setChecked(isChecked);
		mediaPlayMode.setOnCheckedChangeListener(mediaPlayModeChangeListener);

		slideshowMode = (ToggleButton) findViewById(R.id.toggle_slideshow);
		slideshowMode.setTag("slideshowMode");
		photoPath = preferences.getString("photoPath", "");
		isChecked = preferences.getBoolean("slideshowMode", false);
		if (isChecked)
			slideshowMode.setTextOn(getString(R.string.slide_show_on) + "\n"
					+ photoPath);
		slideshowMode.setChecked(isChecked);
		slideshowMode.setOnCheckedChangeListener(slideshowModeChangeListener);

		videoMode = (ToggleButton) findViewById(R.id.toggle_video);
		videoMode.setTag("videoMode");
		videoPath = preferences.getString("videoPath", "");
		isChecked = preferences.getBoolean("videoMode", false);
		if (isChecked)
			videoMode
					.setTextOn(getString(R.string.video_on) + "\n" + videoPath);
		videoMode.setChecked(isChecked);
		videoMode.setOnCheckedChangeListener(videoModeChangeListener);

		urlMode = (ToggleButton) findViewById(R.id.toggle_url);
		urlMode.setTag("urlMode");
		urlMode.setChecked(preferences.getBoolean("urlMode", false));
		urlMode.setOnCheckedChangeListener(urlModeChangeListener);
		urlPath = preferences.getString("urlPath", "");

		prefixMode = (ToggleButton) findViewById(R.id.toggle_prefix);
		prefixMode.setTag("prefixMode");
		prefixMode.setChecked(preferences.getBoolean("prefixMode", false));
		prefixMode.setOnCheckedChangeListener(prefixModeChangeListener);
		prefix = preferences.getString("prefix", "");
		if (prefix.length() == 0) {
			prefix = getString(R.string.default_prefix);
			preferences.edit().putString("prefix", prefix).commit();
		}

		suffixMode = (ToggleButton) findViewById(R.id.toggle_suffix);
		suffixMode.setTag("suffixMode");
		suffixMode.setChecked(preferences.getBoolean("suffixMode", false));
		suffixMode.setOnCheckedChangeListener(suffixModeChangeListener);
		suffix = preferences.getString("suffix", "");
		if (suffix.length() == 0) {
			suffix = getString(R.string.default_suffix);
			preferences.edit().putString("suffix", suffix).commit();
		}

		debugMode = (ToggleButton) findViewById(R.id.toggle_debug);
		debugMode.setOnCheckedChangeListener(normalModeChangeListener);
		debugMode.setTag("debugMode");
		debugMode.setChecked(preferences.getBoolean("debugMode", false));

		menuSlideshowMode = (ToggleButton) findViewById(R.id.toggle_menu_slideshow);
		menuSlideshowMode.setTag("menuSlideshowMode");
		menuPhotoPath = preferences.getString("menuPhotoPath", "");
		isChecked = preferences.getBoolean("menuSlideshowMode", false);
		if (isChecked)
			menuSlideshowMode.setTextOn(getString(R.string.slide_show_on)
					+ "\n" + menuPhotoPath);
		menuSlideshowMode.setChecked(isChecked);
		menuSlideshowMode
				.setOnCheckedChangeListener(menuSlideshowModeChangeListener);

		menuVideoMode = (ToggleButton) findViewById(R.id.toggle_menu_video);
		menuVideoMode.setTag("menuVideoMode");
		menuVideoPath = preferences.getString("menuVideoPath", "");
		isChecked = preferences.getBoolean("menuVideoMode", false);
		if (isChecked)
			menuVideoMode.setTextOn(getString(R.string.video_on) + "\n"
					+ menuVideoPath);
		menuVideoMode.setChecked(isChecked);
		menuVideoMode.setOnCheckedChangeListener(menuVideoModeChangeListener);

		menuUrlMode = (ToggleButton) findViewById(R.id.toggle_menu_url);
		menuUrlMode.setTag("menuUrlMode");
		menuUrlMode.setChecked(preferences.getBoolean("menuUrlMode", false));
		menuUrlMode.setOnCheckedChangeListener(menuUrlModeChangeListener);
		menuUrlPath = preferences.getString("menuUrlPath", "");

		Button sender = (Button) findViewById(R.id.button_sender);
		sender.setOnClickListener(senderOnClick);

		Button receiver = (Button) findViewById(R.id.button_receiver);
		receiver.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startReceiver();
			}
		});

		Button menuBoard = (Button) findViewById(R.id.button_menu_board);
		menuBoard.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startMenuBoard();
			}
		});

		Button exit = (Button) findViewById(R.id.button_exit);
		exit.setOnClickListener(exitOnClick);

		Button reboot = (Button) findViewById(R.id.button_reboot);
		reboot.setOnClickListener(rebootOnClick);

		ToggleButton autoReboot = (ToggleButton) findViewById(R.id.toggle_auto_reboot);
		autoReboot.setTag("autoRebootMode");
		autoReboot.setChecked(preferences.getBoolean("autoRebootMode", false));
		autoReboot.setOnCheckedChangeListener(autoRebootModeChangeListener);

		backgroundVolume = (SeekBar) findViewById(R.id.seek_music_volume);
		backgroundVolume.setProgress(preferences.getInt("backgroundVolume", 0));
		backgroundVolume.setTag("backgroundVolume");
		backgroundVolume.setOnSeekBarChangeListener(volumeChangeListener);

		if (getIntent().getBooleanExtra("goToLastActivity", true)) {
			int lastScreen = preferences.getInt("lastScreen", 0);
			if (lastScreen > 0) {
				switch (lastScreen) {
				case 1:
					startSimpleSender();
					break;
				case 2:
					startAdvanceSender();
					break;
				case 3:
					startReceiver();
					break;
				case 4:
					startMenuBoard();
					break;
				}
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case 0:
			filePath = data.getStringExtra(FileDialog.RESULT_PATH);
			mediaPlayMode.setTextOn(getString(R.string.media_folder) + "\n"
					+ filePath);
			preferences.edit().putString("filePath", filePath).commit();

			// Force redraw the new text
			mediaPlayMode.setChecked(true);
			break;
		case 1:
			photoPath = data.getStringExtra(FileDialog.RESULT_PATH);
			slideshowMode.setTextOn(getString(R.string.slide_show_on) + "\n"
					+ photoPath);
			preferences.edit().putString("photoPath", photoPath).commit();
			// Force redraw the new text
			slideshowMode.setChecked(true);
			break;
		case 2:
			videoPath = data.getStringExtra(FileDialog.RESULT_PATH);
			videoMode
					.setTextOn(getString(R.string.video_on) + "\n" + videoPath);
			preferences.edit().putString("videoPath", videoPath).commit();
			// Force redraw the new text
			videoMode.setChecked(true);
			break;
		case 10:
			menuPhotoPath = data.getStringExtra(FileDialog.RESULT_PATH);
			menuSlideshowMode.setTextOn(getString(R.string.slide_show_on)
					+ "\n" + menuPhotoPath);
			preferences.edit().putString("menuPhotoPath", menuPhotoPath)
					.commit();
			// Force redraw the new text
			menuSlideshowMode.setChecked(true);
			break;
		case 11:
			menuVideoPath = data.getStringExtra(FileDialog.RESULT_PATH);
			menuVideoMode.setTextOn(getString(R.string.video_on) + "\n"
					+ menuVideoPath);
			preferences.edit().putString("menuVideoPath", menuVideoPath)
					.commit();
			// Force redraw the new text
			menuVideoMode.setChecked(true);
			break;

		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		preferences.edit().putInt("lastScreen", 0).commit();
	}

	/**
	 * Prepare parameter for an intent.
	 * 
	 * @param i
	 */
	private void prepareIntent(Intent i) {
		if (mediaPlayMode.isChecked())
			i.putExtra("mediaPath", filePath);
		i.putExtra("prefixMode", prefixMode.isChecked());
		i.putExtra("suffixMode", suffixMode.isChecked());
		i.putExtra("prefix", prefix);
		i.putExtra("suffix", suffix);
	}

	private void startReceiver() {
		Intent i = new Intent(MainActivity.this, ReceiverActivity.class);
		prepareIntent(i);
		i.putExtra("videoMode", videoMode.isChecked());
		i.putExtra("videoPath", videoPath);
		i.putExtra("debugMode", debugMode.isChecked());
		i.putExtra("slideshowMode", slideshowMode.isChecked());
		i.putExtra("photoPath", photoPath);
		i.putExtra("urlMode", urlMode.isChecked());
		i.putExtra("url", urlPath);
		startActivity(i);
	}

	private void startMenuBoard() {
		Intent i = new Intent(MainActivity.this, MenuBoardActivity.class);
		i.putExtra("slideshowMode", menuSlideshowMode.isChecked());
		i.putExtra("photoPath", menuPhotoPath);
		i.putExtra("videoMode", menuVideoMode.isChecked());
		i.putExtra("videoPath", menuVideoPath);
		i.putExtra("urlMode", menuUrlMode.isChecked());
		i.putExtra("url", menuUrlPath);
		startActivity(i);
	}

	private void startSimpleSender() {
		Intent i = new Intent(MainActivity.this, SimpleSenderActivity.class);
		prepareIntent(i);
		MainActivity.this.startActivity(i);

	}

	private void startAdvanceSender() {
		Intent i = new Intent(MainActivity.this, SenderActivity.class);
		prepareIntent(i);
		MainActivity.this.startActivity(i);
	}
}
