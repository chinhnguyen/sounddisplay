package com.nexmac.sounddisplay;

import java.io.IOException;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class SoundDisplayApp extends Application {
	private static final String ID = "com.nexmac.sounddisplay.reboot";
	private static final String TAG = "SoundDisplayApp";

	private AlarmManager alarmManager;
	private PendingIntent rebootIntent;
	private SharedPreferences preferences;

	@Override
	public void onCreate() {
		super.onCreate();
		// Create global configuration and initialize ImageLoader with this
		// configuration
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.EXACTLY).build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext()).threadPoolSize(1)
				.defaultDisplayImageOptions(defaultOptions)
				.denyCacheImageMultipleSizesInMemory().build();
		ImageLoader.getInstance().init(config);

		alarmManager = (AlarmManager) this
				.getSystemService(Context.ALARM_SERVICE);
		// Create pending intent for registering with alarm manager
		rebootIntent = PendingIntent.getBroadcast(this, 0, new Intent(ID), 0);

		// Register next reboot time
		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);
		if (preferences.getBoolean("autoRebootMode", false))
			registerAutoReboot();
	}

	public void unregisterAutoReboot() {
		alarmManager.cancel(rebootIntent);
	}

	public void registerAutoReboot() {
		// Reboot receiver
		BroadcastReceiver rebootReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				try {
					// Cancel the alarm - so it wont get fired twice
					alarmManager.cancel(rebootIntent);
					Log.i(TAG, "Reboot at - "
							+ Calendar.getInstance().toString());
					// Check before reboot, just to besure if un-registering
					// doesn't work
					if (preferences.getBoolean("autoRebootMode", false))
						// Reboot
						Runtime.getRuntime().exec(
								new String[] { "su", "-c", "reboot" });
				} catch (IOException e) {
					Log.e(TAG, "Reboot failed");
					Log.e(TAG, e.toString());
				}
			}
		};
		// Register receiver with context
		registerReceiver(rebootReceiver, new IntentFilter(ID));

		Calendar nextRebootTime = Calendar.getInstance();
		// TEST - reboot 10 seconds after opened
		// nextRebootTime.add(Calendar.SECOND, 10);
		// One day from now
		nextRebootTime.add(Calendar.DATE, 1);
		// At 3 am in the morning
		nextRebootTime.set(Calendar.HOUR, 3);
		nextRebootTime.set(Calendar.MINUTE, 0);
		nextRebootTime.set(Calendar.SECOND, 0);
		// Make sure there is no stupid reboot
		if (nextRebootTime.after(Calendar.getInstance()))
			alarmManager.set(AlarmManager.RTC_WAKEUP,
					nextRebootTime.getTimeInMillis(), rebootIntent);
	}
}
