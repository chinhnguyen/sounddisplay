package com.nexmac.sounddisplay;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewFlipper;

import com.nexmac.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("DefaultLocale")
public class ReceiverActivity extends SoundActivity {

	// The main text view
	private TextView numberText;
	// The main text view
	private TextView serverInfoText;
	// The number buffer text
	private TextView bufferText;
	// The number buffer text
	private TextView memoryText;
	// Input text
	private TextView inputText;

	// Ad text size
	private TextView adText;
	// Row of numbers
	private View numberRow;
	// List of text views
	private TextView[] bottomTextViews = new TextView[6];
	// List of received and processed number
	private ArrayList<Integer> numbersList = new ArrayList<Integer>();
	// Handling the received (not yet processed) numbered
	private ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(
			1000);

	// Image slide show
	private ViewFlipper imageSlideShow;

	// Video view
	private VideoView videoView;
	private MediaPlayer mediaPlayer;
	private WebView webView;

	private boolean shouldSlideshowContinue;
	// Handler for running slide show
	private Handler slideshowHandler = new Handler();

	// Runnable for running slide show
	private Runnable slideshowRunnable = new Runnable() {

		@Override
		public void run() {
			if (!shouldSlideshowContinue)
				return;
			// Show next image
			imageSlideShow.showNext();

			ImageView imageView = (ImageView) imageSlideShow.getCurrentView();
			File f = (File) imageView.getTag();
			if (f == null)
				return;

			if (debugMode) {
				Toast.makeText(ReceiverActivity.this, f.getPath(),
						Toast.LENGTH_LONG).show();
			}

			// Load images using Image Loader
			ImageLoader.getInstance().displayImage(
					"file://" + f.toURI().getPath(), imageView);
			// Raise next event
			if (imageSlideShow.getChildCount() > 1)
				slideshowHandler.postDelayed(slideshowRunnable, 5000);
		}
	};

	// For monitoring memory usage
	private Handler memoryMonitorHandler = new Handler();

	// For monitor memory usage
	private Runnable memoryMonitorRunnable = new Runnable() {
		// http://stackoverflow.com/questions/6766898/monitor-the-memory-ocuppied-by-my-app-in-android
		@SuppressLint("DefaultLocale")
		@Override
		public void run() {
			Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
			Debug.getMemoryInfo(memoryInfo);

			final String memMessage = String.format(
					"App Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
					memoryInfo.getTotalPss() / 1024.0,
					memoryInfo.getTotalPrivateDirty() / 1024.0,
					memoryInfo.getTotalSharedDirty() / 1024.0);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					memoryText.setText(memMessage);
					memoryMonitorHandler
							.postDelayed(memoryMonitorRunnable, 100);
				}
			});
		}
	};

	/*
	 * Listen to number over the network
	 */
	private NumberDatagramListener numberDatagramListener;

	// Whether receiver is running in debug mode or not
	private boolean debugMode;

	private SharedPreferences preferences;

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		preferences = getSharedPreferences("com.nexmac.sounddisplay",
				MODE_PRIVATE);

		activated = preferences.getBoolean("activated", false);

		// http://stackoverflow.com/questions/2039555/how-to-get-an-android-wakelock-to-work
		// Keep the screen always ON
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LOW_PROFILE);

		setContentView(R.layout.activity_receiver);

		// Get the text view
		numberText = (TextView) findViewById(R.id.text_number);

		serverInfoText = (TextView) findViewById(R.id.text_info);
		bufferText = (TextView) findViewById(R.id.text_buffer);
		memoryText = (TextView) findViewById(R.id.text_memory);
		inputText = (TextView) findViewById(R.id.text_input_number);
		// Clear info
		numberText.setText("");
		serverInfoText.setText("Orders:");
		bufferText.setText("");
		inputText.setText("");
		// Init the list of text views
		for (int i = 1; i <= 6; i++) {
			Integer id = getResources().getIdentifier("text_" + i, "id",
					getBaseContext().getPackageName());
			TextView v = (TextView) findViewById(id);
			v.setText("");
			bottomTextViews[i - 1] = v;
		}

		// Prepare number animation for number text
		runNumberAnimation();

		// Init for debug mode
		debugMode = getIntent().getBooleanExtra("debugMode", false);
		if (debugMode) {
			// Start updating memory
			memoryMonitorHandler.post(memoryMonitorRunnable);
		} else {
			serverInfoText.setVisibility(View.GONE);
			bufferText.setVisibility(View.GONE);
			memoryText.setVisibility(View.GONE);
		}

		// Playing slideshow or video
		boolean slideshowMode = getIntent().getBooleanExtra("slideshowMode",
				false);
		if (slideshowMode) {
			initSlideshow(getIntent().getStringExtra("photoPath"));
		} else {
			boolean videoMode = getIntent().getBooleanExtra("videoMode", false);
			if (videoMode) {
				initVideo(getIntent().getStringExtra("videoPath"));
			} else {
				initWeb(getIntent().getStringExtra("url"));
			}
		}

		// Run number listener's thread
		numberListener.start();

		adText = (TextView) findViewById(R.id.text_ad);
		if (activated)
			adText.setText(getResources().getText(R.string.pickup_ready));

		numberRow = findViewById(R.id.row_orders);
		runAdAnimation();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (videoView != null)
			videoView.pause();
		else if (mediaPlayer != null && mediaPlayer.isPlaying())
			mediaPlayer.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (videoView != null && !videoView.isPlaying())
			videoView.start();
		else if (mediaPlayer != null && !mediaPlayer.isPlaying())
			mediaPlayer.start();
		if (numberDatagramListener == null) {
			// Run number datagram listener's thread
			numberDatagramListener = new NumberDatagramListener();
			numberDatagramListener.start();
		}
		preferences.edit().putInt("lastScreen", 3).commit();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (numberDatagramListener != null) {
			numberDatagramListener.stopServer();
			numberDatagramListener = null;
		}
		shouldSlideshowContinue = false;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (numberDatagramListener != null) {
			numberDatagramListener.stopServer();
			numberDatagramListener = null;
		}
		shouldSlideshowContinue = false;
	}

	private String numText = "";
	private String passText = "";
	private boolean activated = false;
	private Timer timer = new Timer();
	private TimerTask sendTimerTask;

	private TimerTask createSendTask() {
		return new TimerTask() {

			@Override
			public void run() {
				if (numText.length() == 0)
					return;
				numberHandler.post(new NumberProcessor(Integer
						.parseInt(numText)));
				numText = "";
				ReceiverActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						inputText.setText(numText);
					}
				});
			}
		};
	}

	private void acceptTypedNumber(int number) {
		if (!activated) {
			passText = passText + number;
			if (passText.length() > 6) {
				passText = passText.substring(passText.length() - 3);
			}
			if (passText.equals("122709")) {
				activated = true;
				preferences.edit().putBoolean("activated", true).commit();
				adText.setText(getResources().getText(R.string.pickup_ready));
			}
			return;
		}

		numText = numText + number;
		if (numText.length() > 3) {
			numText = numText.substring(numText.length() - 3);
		}
		inputText.setText(numText);
		// Cancel timer
		if (sendTimerTask != null) {
			sendTimerTask.cancel();
			timer.purge();
		}
		// Schedule another timer action
		timer.schedule(sendTimerTask = createSendTask(), 2000);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
			acceptTypedNumber(keyCode - KeyEvent.KEYCODE_0);
		} else if (keyCode >= KeyEvent.KEYCODE_NUMPAD_0
				&& keyCode <= KeyEvent.KEYCODE_NUMPAD_9) {
			acceptTypedNumber(keyCode - KeyEvent.KEYCODE_NUMPAD_0);
		} else if (keyCode == KeyEvent.KEYCODE_ENTER && numText.length() > 0) {
			numberHandler.post(new NumberProcessor(Integer.parseInt(numText)));
			numText = "";
			inputText.setText(numText);
			// Cancel timer
			if (sendTimerTask != null) {
				sendTimerTask.cancel();
				timer.purge();
			}
		}
		return super.onKeyUp(keyCode, event);
	}

	private Animation numberInAnim;
	private Animation numberOutAnim;

	private void runNumberAnimation() {

		if (numberInAnim == null) {
			// Big number animation
			numberInAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.slide_in_left);
			numberOutAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.slide_out_right);

			numberInAnim.setDuration(1000);
			numberInAnim.setInterpolator(new OvershootInterpolator());
			numberInAnim.setFillAfter(true);

			numberOutAnim.setDuration(1000);
			numberOutAnim
					.setInterpolator(new AnticipateOvershootInterpolator());
			numberOutAnim.setFillAfter(true);
			numberOutAnim.setStartOffset(5000);

			numberInAnim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation arg0) {
					numberText.startAnimation(numberOutAnim);
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
				}

				@Override
				public void onAnimationStart(Animation arg0) {
				}
			});
			//
			// outAnim.setAnimationListener(new AnimationListener() {
			//
			// @Override
			// public void onAnimationEnd(Animation arg0) {
			// numberText.startAnimation(inAnim);
			// }
			//
			// @Override
			// public void onAnimationRepeat(Animation arg0) {
			// }
			//
			// @Override
			// public void onAnimationStart(Animation arg0) {
			// }
			// });
		}
		// clear old animation
		numberText.clearAnimation();
		// begin new animation
		numberText.startAnimation(numberInAnim);
	}

	private Animation adInAnim;
	private Animation adOutAnim;

	private Animation numberRowInAnim;
	private Animation numberRowOutAnim;

	private void runAdAnimation() {

		if (adInAnim == null) {
			int stayTime = getResources().getInteger(
					R.integer.receiver_ad_staying_time) * 1000;
			int animationTime = getResources().getInteger(
					R.integer.receiver_ad_animation_time) * 1000;

			// Big number animation
			adInAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.fade_in);
			adOutAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.fade_out);

			adInAnim.setDuration(animationTime);
			adInAnim.setFillAfter(true);
			adInAnim.setStartOffset(stayTime);

			adOutAnim.setDuration(animationTime);
			adOutAnim.setFillAfter(true);
			adOutAnim.setStartOffset(stayTime);

			// Row number animation
			numberRowInAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.fade_in);
			numberRowOutAnim = AnimationUtils.loadAnimation(this,
					android.R.anim.fade_out);
			numberRowInAnim.setDuration(animationTime);
			numberRowInAnim.setFillAfter(true);
			numberRowInAnim.setStartOffset(stayTime);
			numberRowOutAnim.setDuration(animationTime);
			numberRowOutAnim.setFillAfter(true);
			numberRowOutAnim.setStartOffset(stayTime);

			adInAnim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation arg0) {
					adText.startAnimation(adOutAnim);
					numberRow.startAnimation(numberRowInAnim);
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
				}

				@Override
				public void onAnimationStart(Animation arg0) {
				}
			});

			adOutAnim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation arg0) {
					adText.startAnimation(adInAnim);
					numberRow.startAnimation(numberRowOutAnim);
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
				}

				@Override
				public void onAnimationStart(Animation arg0) {
				}
			});
		}
		// clear old animation
		adText.clearAnimation();
		// clear old animation
		numberRow.clearAnimation();
		// begin new animation
		adText.startAnimation(adInAnim);
	}

	/**
	 * 
	 * @param flipper
	 * @param parent
	 */
	@SuppressLint("DefaultLocale")
	private void addFlipperImages(final ViewFlipper flipper, File[] files) {
		// ImageLoader.getInstance().clearMemoryCache();
		// Make sure the folder exists
		if (files.length == 0)
			return;
		// Get count of files
		int imageCount = files.length;
		// Prepare the params for adding new image view
		ViewFlipper.LayoutParams params = new ViewFlipper.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		// Add all images using Image loader
		for (int i = 0; i < imageCount; i++) {
			File f = files[i];
			// Accept images file only
			if (f.isDirectory()
					|| !(f.getName().toLowerCase().endsWith(".jpg")
							|| f.getName().toLowerCase().endsWith(".png") || f
							.getName().toLowerCase().endsWith(".gif")))
				continue;

			// New image view
			final ImageView imageView = new ImageView(this);
			// For UIL co calculate the size
			imageView.setMaxWidth(metrics.widthPixels);
			imageView.setMaxHeight(metrics.heightPixels);
			// Should stretch to fit screen
			imageView.setScaleType(ScaleType.FIT_XY);
			// Add it to flipper
			imageView.setLayoutParams(params);
			imageView.setTag(f);
			// Add to flipper only when it is success
			flipper.addView(imageView);
		}
	}

	/**
	 * Display number to bottom panel. Notify the number listener to play the
	 * next number if any.
	 * 
	 * @param number
	 */
	@Override
	protected void onFinishPlayingSound(final Integer number) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (mediaPlayer != null) {
					mediaPlayer.setVolume(volume, volume);
				}

				if (!numbersList.contains(number)) {
					// Remove the last item if it is more than 6 elements
					if (numbersList.size() == 6)
						numbersList.remove(numbersList.size() - 1);
					// Add to list of displaying number
					numbersList.add(0, number);
					// Update display list
					for (int i = 0; i < 6; i++) {
						if (i < numbersList.size()) {
							bottomTextViews[i].setText(String.format("#%d",
									numbersList.get(i)));
						} else {
							bottomTextViews[i].setText("");
						}
					}
				}
				// Notify listener that it should process the next number
				synchronized (numberListener) {
					numberListener.notify();
				}
			}
		});
	}

	/**
	 * Init Receiver to play slide show
	 * 
	 * @param path
	 */
	private void initSlideshow(String path) {
		if (TextUtils.isEmpty(path))
			return;
		// Init slide shod
		imageSlideShow = (ViewFlipper) findViewById(R.id.view_slide_show);
		imageSlideShow.setVisibility(View.VISIBLE);
		// Get images folder
		File imagesFolder = new File(path);
		if (!imagesFolder.exists())
			return;
		File[] files = imagesFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				String lowercase = name.toLowerCase();
				return lowercase.endsWith(".png") || lowercase.endsWith(".jpg")
						|| lowercase.endsWith(".gif");
			}
		});
		if (files == null || files.length == 0)
			return;
		addFlipperImages(imageSlideShow, files);

		imageSlideShow.setInAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_in));
		imageSlideShow.setOutAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_out));

		// Begin the timing
		shouldSlideshowContinue = true;
		slideshowHandler.postDelayed(slideshowRunnable, 500);

		// Init sounds
		initSlideshowSounds(imagesFolder);
	}

	private int soundIndex = 0;

	/**
	 * Play sounds for slideshow
	 * 
	 * @param files
	 */
	private void initSlideshowSounds(File slideshows) {
		if (!slideshows.exists())
			return;
		final File[] soundFiles = slideshows.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				String lowercase = name.toLowerCase();
				return lowercase.endsWith(".mp3");
			}
		});
		if (soundFiles.length == 0)
			return;
		try {
			// Sort by name
			Arrays.sort(soundFiles);
			// New media player
			mediaPlayer = new MediaPlayer();
			// For getting the media player
			mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mediaPlayer = mp;
					mediaPlayer.setVolume(volume, volume);
				}
			});

			// Register on completion listener to play next video
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					soundIndex++;
					if (soundIndex == soundFiles.length)
						soundIndex = 0;
					try {
						mediaPlayer.reset();
						mediaPlayer.setDataSource(soundFiles[soundIndex]
								.getAbsolutePath());
						mediaPlayer.prepare();
						mediaPlayer.start();
					} catch (IOException e) {
						Log.e("Receiver",
								"Error occurred during playing sounds", e);
					}
				}
			});
			// Play the first sound
			mediaPlayer.setDataSource(soundFiles[soundIndex].getAbsolutePath());
			// Prepare
			mediaPlayer.prepare();
			// Play
			mediaPlayer.start();
		} catch (Exception e) {
			Log.e("Receiver", "Error occurred during playing sounds", e);
		}
	}

	private int videoIndex = 0;

	/**
	 * Init playing of videos
	 * 
	 * @param path
	 */
	private void initVideo(final String path) {
		if (TextUtils.isEmpty(path))
			return;
		videoView = (VideoView) findViewById(R.id.view_video);
		videoView.setVisibility(View.VISIBLE);

		// For getting the media player
		videoView.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				mediaPlayer = mp;
				mediaPlayer.setVolume(volume, volume);
			}
		});

		// Get videos folder
		File file = new File(path);
		if (!file.exists())
			return;
		File videofiles[];
		if (file.isDirectory()) {
			videofiles = file.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					String lowercase = name.toLowerCase();
					return lowercase.endsWith(".mp4")
							|| lowercase.endsWith(".3gp")
							|| lowercase.endsWith(".ogg")
							|| lowercase.endsWith(".mpg");
				}
			});
		} else {
			String lowercase = file.getName().toLowerCase();
			if (!(lowercase.endsWith(".mp4") || lowercase.endsWith(".3gp") || lowercase
					.endsWith(".ogg") || lowercase.endsWith(".mpg"))) {
				return;
			}
			videofiles = new File[] { file };
		}
		if (videofiles == null || videofiles.length == 0)
			return;
		final File[] files = videofiles;
		try {
			// Sort by name
			Arrays.sort(files);
			// Register on completion listener to play next video
			videoView.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					videoIndex++;
					if (videoIndex == files.length)
						videoIndex = 0;
					videoView.setVideoPath(files[videoIndex].toString());
					videoView.start();
				}
			});
			// Play the first video
			videoView.setVideoPath(files[videoIndex].toString());
			videoView.start();
		} catch (Exception e) {
			Log.e("Receiver", "Error occurred during playing videos", e);
		}
	}

	private void fallbackToSlideshow(String message) {
		initSlideshow(getIntent().getStringExtra("photoPath"));
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWeb(final String url) {
		if (!Utils.isConnected(this)) {
			fallbackToSlideshow("Network is not available, fallback to slideshow!");
			return;
		}
		if (TextUtils.isEmpty(url)) {
			fallbackToSlideshow("URL was not given, fallback to slideshow!");
			return;
		}
		webView = (WebView) findViewById(R.id.view_web);
		webView.setVisibility(View.VISIBLE);
		webView.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					onKeyUp(keyCode, event);
				}
				return false;
			}
		});

		// http://stackoverflow.com/questions/4066438/android-webview-how-to-handle-redirects-in-app-instead-of-opening-a-browser
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				webView.setVisibility(View.GONE);
				fallbackToSlideshow("Could not load URL " + failingUrl
						+ ", fallback to slideshow!");
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.clearCache(true);
			}
		});

		// http://stackoverflow.com/questions/3916330/android-webview-webpage-should-fit-the-device-screen
		webView.setInitialScale(1);
		webView.setPadding(0, 0, 0, 0);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webView.setScrollbarFadingEnabled(false);
		// http://stackoverflow.com/questions/7537701/prevent-android-webview-caching-data
		Map<String, String> noCacheHeaders = new HashMap<String, String>(2);
		noCacheHeaders.put("Pragma", "no-cache");
		noCacheHeaders.put("Cache-Control", "no-cache");
		webView.loadUrl(url, noCacheHeaders);
	}

	private void setStatus(final String message) {
		// Update server info text
		runOnUiThread(new Runnable() {
			@SuppressLint("DefaultLocale")
			@Override
			public void run() {
				serverInfoText.setText(message);
			}
		});
	}

	private void setErrorStatus(final String message) {
		runOnUiThread(new Runnable() {
			@SuppressLint("DefaultLocale")
			@Override
			public void run() {
				serverInfoText.setText(message);
				serverInfoText.setTextColor(ReceiverActivity.this
						.getResources().getColor(R.color.error_text));
			}
		});
	}

	/**
	 * Listen to number from the network.
	 */
	private class NumberDatagramListener extends Thread {
		DatagramSocket serverSocket;

		public void stopServer() {
			if (serverSocket != null)
				serverSocket.close();
		}

		@Override
		public void run() {

			try {
				serverSocket = new DatagramSocket(RECEIVER_PORT);
			} catch (SocketException e) {
				// Log
				Log.e("Receiver", e.toString());
				// Further UI exception handling
				// Update server info text
				setErrorStatus(String.format("Could not start server on %s:%d",
						Utils.getIPAddress(true), RECEIVER_PORT));
				return;
			}

			setStatus(String.format("Listening on %s:%d",
					Utils.getIPAddress(true), RECEIVER_PORT));

			// Int number buffer
			byte[] buf = new byte[4];
			// Datagram packet to get an int
			DatagramPacket pack = new DatagramPacket(buf, buf.length);
			while (!serverSocket.isClosed()) {
				try {
					// Wait for package
					serverSocket.receive(pack);
					// Do nothing the socket is closed
					if (serverSocket.isClosed())
						break;
					// Get the number
					int number = ByteBuffer.wrap(buf).getInt();
					// Make sure not out of range
					if (number < 0 || number > 999)
						continue;
					try {
						// Put the number of queue for processing
						// OR wait for the queue has blank space
						queue.put(number);
						// Update text (only for DEBUG)
						updateQueueNumbersInfo();
					} catch (InterruptedException e) {
					}
				} catch (IOException e) {
					// Log
					Log.e("Receiver", e.toString());
					// Further UI exception handling
					handleException(e);
				}
			}
		}
	}

	/**
	 * Update the list of received numbers
	 */
	private void updateQueueNumbersInfo() {
		// Only run in debug mode
		if (!debugMode)
			return;

		Iterator<Integer> i = queue.iterator();
		StringBuilder sb = new StringBuilder();
		while (i.hasNext()) {
			sb.append("-" + i.next());
		}
		final String message = "Orders: "
				+ (sb.length() > 0 ? sb.substring(1) : "");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				bufferText.setText(message);
			}
		});
	}

	/**
	 * Listen for new number and do any processing possible
	 */
	private Thread numberListener = new Thread() {

		@Override
		public void run() {
			while (true) {
				try {
					// Take the last number out of queue if there is already
					// number in here
					// OR wait for the new number to come
					final int number = queue.take();

					synchronized (this) {
						// Inform number handler about a new number
						numberHandler.post(new NumberProcessor(number));
						// Wait for the number to finish processing
						wait();
						// Update the debug text
						updateQueueNumbersInfo();
					}
				} catch (InterruptedException e) {
					// Should never goes here
				}
			}
		}
	};

	/**
	 * For handling number received from senders.
	 */
	private Handler numberHandler = new Handler();

	/**
	 * Number processor: 1. Display big number 2. Play sound 3. Wait for sound
	 * to finish and displaying on screen number
	 * 
	 * @author ntr9h
	 * 
	 */
	private class NumberProcessor implements Runnable {
		int number;

		public NumberProcessor(int number) {
			this.number = number;
		}

		@Override
		public void run() {
			// Set display text
			numberText.setText(String.format("%d", number));
			// Reset animation with new number
			runNumberAnimation();

			if (mediaPlayer != null) {
				mediaPlayer.setVolume(0.5f * volume, 0.5f * volume);
			}
			// Play sound
			playNumber(number);
		}
	};
}
