package com.nexmac.sounddisplay.models;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;

public class Order implements Parcelable {
	public Integer order;
	public String note;
	public OrderItem[] description;

	public Order() {

	}

	public Order(Parcel in) {
		order = in.readInt();
		note = in.readString();
		description = (OrderItem[]) in.readParcelableArray(OrderItem.class
				.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(order);
		dest.writeString(note);
		dest.writeParcelableArray(description, flags);
	}

	public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
		public Order createFromParcel(Parcel in) {
			return new Order(in);
		}

		public Order[] newArray(int size) {
			return new Order[size];
		}
	};

	public void writeJson(JsonWriter jw) throws IOException {
		jw.beginObject();
		jw.name("order").value(order);
		jw.name("note").value(note);
		jw.name("description");
		jw.beginArray();
		for (int i = 0; i < description.length; i++) {
			OrderItem item = description[i];
			item.writeJson(jw);
		}
		jw.endArray();
		jw.endObject();
	}

	public void readJson(JsonReader jr) throws IOException {
		ArrayList<OrderItem> items = new ArrayList<OrderItem>();
		jr.beginObject();
		while (jr.hasNext()) {
			String name = jr.nextName();
			if (name.equals("order")) {
				order = jr.nextInt();
			} else if (name.equals("note")) {
				note = jr.nextString();
			} else if (name.equals("description")) {
				jr.beginArray();
				while (jr.hasNext()) {
					items.add(OrderItem.createFromJson(jr));
				}
				jr.endArray();
			} else {
				jr.skipValue();
			}
		}
		jr.endObject();
		description = items.toArray(new OrderItem[] {});
	}

	public static String toJson(Order[] orders) {
		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw);
		try {
			jw.beginArray();
			for (int i = 0; i < orders.length; i++) {
				Order order = orders[i];
				order.writeJson(jw);
			}
			jw.endArray();
		} catch (IOException e) {
			Log.e("ParseJson", e.toString());
			return null;
		} finally {
			try {
				jw.close();
			} catch (IOException e) {
				Log.e("ParseJson", e.toString());
				return null;
			}
		}
		return sw.toString();
	}

	public static Order[] arrayFromJson(String json) {
		ArrayList<Order> orders = new ArrayList<Order>();

		JsonReader jr = new JsonReader(new StringReader(json));
		try {
			jr.beginArray();
			while (jr.hasNext()) {
				Order o = new Order();
				o.readJson(jr);
				orders.add(o);
			}
			jr.endArray();
		} catch (IOException e) {
			Log.e("ParseJson", e.toString());
		} finally {
			try {
				jr.close();
			} catch (IOException e) {
				Log.e("ParseJson", e.toString());
			}
		}
		return orders.toArray(new Order[] {});
	}

	public static Order fromJson(String json) {
		Order order = new Order();

		JsonReader jr = new JsonReader(new StringReader(json));
		try {
			order.readJson(jr);
		} catch (IOException e) {
			Log.e("ParseJson", e.toString());
		} finally {
			try {
				jr.close();
			} catch (IOException e) {
				Log.e("ParseJson", e.toString());
			}
		}

		return order;
	}
}
