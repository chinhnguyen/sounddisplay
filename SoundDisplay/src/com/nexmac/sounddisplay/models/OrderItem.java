package com.nexmac.sounddisplay.models;

import java.io.IOException;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonWriter;

public class OrderItem implements Parcelable {
	public int index;
	public String name;
	public int quantity;

	public void writeJson(JsonWriter jw) throws IOException {
		jw.beginObject();
		jw.name("index").value(index);
		jw.name("name").value(name);
		jw.name("quantity").value(quantity);
		jw.endObject();
	}

	public static OrderItem createFromJson(JsonReader jr) throws IOException {
		OrderItem item = new OrderItem();
		jr.beginObject();

		while (jr.hasNext()) {
			String itemName = jr.nextName();
			if (itemName.equals("index")) {
				item.index = jr.nextInt();
			} else if (itemName.equals("name")) {
				item.name = jr.nextString();
			} else if (itemName.equals("quantity")) {
				item.quantity = jr.nextInt();
			} else {
				jr.skipValue();
			}
		}
		jr.endObject();
		return item;
	}

	public OrderItem() {

	}

	public OrderItem(Parcel in) {
		index = in.readInt();
		name = in.readString();
		quantity = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(index);
		dest.writeString(name);
		dest.writeInt(quantity);
	}

	public static final Parcelable.Creator<OrderItem> CREATOR = new Parcelable.Creator<OrderItem>() {
		public OrderItem createFromParcel(Parcel in) {
			return new OrderItem(in);
		}

		public OrderItem[] newArray(int size) {
			return new OrderItem[size];
		}
	};
}
