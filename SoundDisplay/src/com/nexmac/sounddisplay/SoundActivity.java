package com.nexmac.sounddisplay;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;

@SuppressWarnings("deprecation")
public abstract class SoundActivity extends FragmentActivity {
	// The port that receiver listen on
	public static final int RECEIVER_PORT = 6000;
	// The port that sender listen on
	public static final int SENDER_PORT = 6001;
	// For playing text to speech
	private TextToSpeech textToSpeech;
	// Status of text to speech
	private int textToSpeechStatus;
	// Status check code
	private int MY_DATA_CHECK_CODE = 0;
	// Store the location of media files
	private String mediaPath;

	private boolean playPrefix;
	private boolean playSuffix;

	private String prefix;
	private String suffix;

	protected float volume;

	/**
	 * Init sound related information
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get alert volume
		SharedPreferences preferences = getSharedPreferences(
				"com.nexmac.sounddisplay", MODE_PRIVATE);
		volume = preferences.getInt("backgroundVolume", 0) / 100f;

		mediaPath = getIntent().getStringExtra("mediaPath");
		playPrefix = getIntent().getBooleanExtra("prefixMode", false);
		playSuffix = getIntent().getBooleanExtra("suffixMode", false);
		prefix = getIntent().getStringExtra("prefix");
		suffix = getIntent().getStringExtra("suffix");

		// check for TTS data
		Intent checkTTSIntent = new Intent();
		checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
	}

	/**
	 * Stop sounds
	 */
	protected void onPause() {
		super.onPause();
		if (player != null && player.isPlaying())
			player.stop();
	}

	/**
	 * act on result of TTS data check
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MY_DATA_CHECK_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// the user has the necessary data - create the TTS
				textToSpeech = new TextToSpeech(this, ttsInitListener);
				// textToSpeech
				// .setOnUtteranceProgressListener(new
				// UtteranceProgressListener() {
				//
				// @Override
				// public void onStart(String utteranceId) {
				// }
				//
				// @Override
				// public void onDone(String utteranceId) {
				// onFinishPlayingSound(0);
				// }
				//
				// @Override
				// public void onError(String utteranceId) {
				// }
				//
				// });

				textToSpeech
						.setOnUtteranceCompletedListener(new OnUtteranceCompletedListener() {

							@Override
							public void onUtteranceCompleted(String utteranceId) {
								onFinishPlayingSound(Integer
										.parseInt(utteranceId));
							}
						});
			} else {
				// no data - install it now
				Intent installTTSIntent = new Intent();
				installTTSIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installTTSIntent);
			}
		}
	}

	/**
	 * Handle Text To Speech initialized.
	 */
	private OnInitListener ttsInitListener = new OnInitListener() {

		@Override
		public void onInit(int status) {
			// check for successful instantiation
			if (textToSpeechStatus == TextToSpeech.SUCCESS) {
				if (textToSpeech.isLanguageAvailable(Locale.US) == TextToSpeech.LANG_AVAILABLE)
					textToSpeech.setLanguage(Locale.US);
			}
		}

	};

	/**
	 * Get called when a sound is finished playing (either TTS or SD file)
	 * 
	 * @param number
	 */
	protected void onFinishPlayingSound(Integer number) {

	}

	// The media player for playing sound
	private MediaPlayer player;
	private int soundIndex;

	/**
	 * Play sound of a number.
	 * 
	 * @param number
	 * @param listener
	 */
	protected void playNumber(Integer number) {
		if (number < 0 || number >= 1000)
			return;

		if (TextUtils.isEmpty(mediaPath)) {
			playNumberTTS(number);
			return;
		}

		final ArrayList<File> files = new ArrayList<File>();

		if (number <= 100) {
			File mediaFile = new File(mediaPath, String.format("%02d.mp3",
					number));
			if (!mediaFile.exists()) {
				playNumberTTS(number);
				return;
			}
			files.add(mediaFile);
		} else {
			File firstMediaFile = new File(mediaPath, String.format("%02d.mp3",
					number / 10));
			File secondMediaFile = new File(mediaPath, String.format(
					"%02d.mp3", number % 10));
			if (!firstMediaFile.exists() || !secondMediaFile.exists()) {
				playNumberTTS(number);
				return;
			}
			files.add(firstMediaFile);
			files.add(secondMediaFile);
		}

		try {
			if (player != null && player.isPlaying()) {
				player.stop();
				player.release();
			}

			final int n = number;
			// Create player with file descriptor
			player = new MediaPlayer();
			player.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					// Increase index
					soundIndex++;
					if (soundIndex == files.size()) {
						// Call back to on finish for the last sound
						onFinishPlayingSound(n);
					} else {
						// Play the next sound
						try {
							player.reset();
							player.setDataSource(files.get(soundIndex)
									.getAbsolutePath());
							// Prepare
							player.prepare();
							// Play
							player.start();
						} catch (IOException e) {
							handleException(e);
						}
					}
				}
			});
			soundIndex = 0;
			// Play from sd card file
			player.setDataSource(files.get(soundIndex).getAbsolutePath());
			// Prepare
			player.prepare();
			player.setVolume(volume, volume);
			player.setLooping(false);
			// Play
			player.start();
		} catch (IOException e) {
			handleException(e);
		}
	}

	/**
	 * Play number using TTS.
	 * 
	 * @param number
	 * @param listener
	 */
	@SuppressLint("DefaultLocale")
	private void playNumberTTS(Integer number) {
		if (textToSpeech == null)
			return;
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
				number.toString());
		// speak straight away
		String text = String.format("%s %d %s", playPrefix ? prefix : "",
				number, playSuffix ? suffix : "");
		textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, parameters);
	}

	/**
	 * Return the broadcast address.
	 * 
	 * @return
	 * @throws IOException
	 */
	private InetAddress getBroadcastAddress() throws IOException {
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow
		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		return InetAddress.getByAddress(quads);
	}

	/**
	 * Handle exception.
	 * 
	 * @param ex
	 */
	protected void handleException(Exception ex) {
		Log.e("SoundActivity", ex.toString());
	}

	/**
	 * Send a number to server.
	 * 
	 * @param number
	 */
	protected void sendToServer(final int number) {
		// Create client thread
		Thread clientThread = new Thread(new Runnable() {

			@Override
			public void run() {
				DatagramSocket socket = null;
				try {
					Random rand = new Random();
					socket = new DatagramSocket(
							rand.nextInt(5999 - 5001) + 5001);
					socket.setBroadcast(true);
					ByteBuffer buffer = ByteBuffer.allocate(4);
					buffer.putInt(number);
					byte[] data = buffer.array();
					DatagramPacket packet = new DatagramPacket(data,
							data.length, getBroadcastAddress(), RECEIVER_PORT);
					socket.send(packet);
				} catch (Exception e) {
					// Print
					e.printStackTrace();
					// UI handling
					handleException(e);
				} finally {
					if (socket != null)
						socket.close();
				}
			}
		});
		// Start client thread
		clientThread.start();
	}
}
