package com.nexmac.test;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Send {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0)
			return;
		ArrayList<String> json = new ArrayList<String>();
		json.add("{\"order\":0,"
				+ "\"note\":\"00000\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Test teo teo\", \"quantity\":1},"
				+ "{\"index\":4,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		json.add("{\"order\":100,"
				+ "\"note\":\"Do this quickly!\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Test teo teo\", \"quantity\":1},"
				+ "{\"index\":4,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		json.add("{\"order\":2,"
				+ "\"note\":\"Do this quickly!\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		json.add("{\"order\":23,"
				+ "\"note\":\"Do this quickly!\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		json.add("{\"order\":44,"
				+ "\"note\":\"Do this quickly!\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		json.add("{\"order\":555,"
				+ "\"note\":\"Do this quickly!\","
				+ "\"description\":"
				+ "["
				+ "{\"index\":1,\"name\":\"Cheese and Bacon Burger dafafasfasfasfasfasfasfasfast\", \"quantity\":2},"
				+ "{\"index\":2,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":3,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":4,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":5,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":6,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":7,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":8,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":9,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":10,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":11,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":12,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":13,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":14,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":15,\"name\":\"Cheese and Bacon Burger\", \"quantity\":2},"
				+ "{\"index\":16,\"name\":\"Chicken Salad\", \"quantity\":1},"
				+ "{\"index\":17,\"name\":\"Coconut Drink\", \"quantity\":3}"
				+ "]" + "}");

		sendJson(json.get(Integer.parseInt(args[0])));
//		 sendNumber(Integer.parseInt(args[0]));
	}

	private static void sendJson(String json) {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket();
			socket.setBroadcast(true);

			InetAddress group = InetAddress.getByName("192.168.2.255");
//			InetAddress group = InetAddress.getByName("10.0.255.255");
			byte[] data = json.getBytes("UTF-8");
			DatagramPacket packet = new DatagramPacket(data, data.length,
					group, 6001);
			socket.send(packet);
		} catch (Exception e) {
			// Print
			e.printStackTrace();
		} finally {
			if (socket != null)
				socket.close();
		}
	}

	private static void sendNumber(int number) {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket();
			socket.setBroadcast(true);
			ByteBuffer buffer = ByteBuffer.allocate(4);
			buffer.putInt(number);
			// buffer.putInt(3);
//			 InetAddress group = InetAddress.getByName("192.168.1.255");

			InetAddress group = InetAddress.getByName("10.0.0.255");
			byte[] data = buffer.array();
			DatagramPacket packet = new DatagramPacket(data, data.length,
					group, 6000);
			socket.send(packet);
		} catch (Exception e) {
			// Print
			e.printStackTrace();
		} finally {
			if (socket != null)
				socket.close();
		}

	}

}
